class Board
  attr_reader :grid

  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def [](pos)
    row,col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row,col = pos
    grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (grid + col + diag).each do |triple|
      return :X if triple == [:X, :X, :X]
      return :O if triple == [:O, :O, :O]
    end

    nil
  end

  def col
    cols = [[],[],[]]
    grid.each_index do |row|
      grid[0].each_index do |col|
        cols[row][col] = grid[col][row]
      end
    end
    cols
  end

  def diag
    dia1 = [[0,0],[1,1],[2,2]]
    dia2 = [[0,2],[1,1],[2,0]]
    [dia1, dia2].map do |dia|
      dia.map {|row,col| grid[row][col]}
    end
  end

  def over?
    grid.flatten.none? {|el| el.nil?} || winner
  end
end
